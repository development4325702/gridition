package com.gridiron.greats.football.stars.players

data class Greats(
    val title: String?,
    val image: Int?,
    val description: String
)
